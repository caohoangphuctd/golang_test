module github.com/akhil/go-bookstore

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.5 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
)
